using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject menuPrincipal;
    public GameObject menuGameOver;

    public float velocidad = 2;
    public GameObject Col;
    public GameObject Piedra1;
    public GameObject Piedra2;
    public Renderer fondo;
    public bool gameOver = false;
    public bool start = false;
    public GameObject volumen;

    public List<GameObject> cols;
    public List<GameObject> obstaculos;
    // Start is called before the first frame update
    void Start()
    {
        volumen.SetActive(false);
        for (int i = 0; i < 21; i++)
        {
            cols.Add(Instantiate(Col, new Vector2(-10 + i, -3), Quaternion.identity));
        }

        obstaculos.Add(Instantiate(Piedra1, new Vector2(13, 0), Quaternion.identity));
        obstaculos.Add(Instantiate(Piedra2, new Vector2(17, 0), Quaternion.identity));
    }

    // Update is called once per frame
    void Update()
    {
        if (start == false)
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                start = true;
            }
        }

        if (start == true && gameOver == true)
        {
            menuGameOver.SetActive(true);
            volumen.SetActive(true);

            if (Input.GetKeyDown(KeyCode.X))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }

        if (start == true && gameOver == false)
        {
            menuPrincipal.SetActive(false);

            fondo.material.mainTextureOffset = fondo.material.mainTextureOffset + new Vector2(0.025f, 0) * Time.deltaTime;

            for (int i = 0; i < cols.Count; i++)
            {
                if (cols[i].transform.position.x <= -10)
                {
                    cols[i].transform.position = new Vector3(10, -3, 0);
                }

                cols[i].transform.position = cols[i].transform.position + new Vector3(-1, 0, 0) * Time.deltaTime * velocidad;
            }

            for (int i = 0; i < obstaculos.Count; i++)
            {
                if (obstaculos[i].transform.position.x <= -20)
                {
                    float randomObs = Random.Range(0, 20);
                    obstaculos[i].transform.position = new Vector3(randomObs, 0, 0);
                }

                obstaculos[i].transform.position = obstaculos[i].transform.position + new Vector3(-1, -0.005f, 0) * Time.deltaTime * velocidad;
            }
        }
    }
}
